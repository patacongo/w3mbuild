/****************************************************************************
 * x3mbuild/stubs/gc.h
 *
 *   Copyright (C) 2018 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef __W3MBBUILD_STUBS_GC_H
#define __W3MBBUILD_STUBS_GC_H 1

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdlib.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define GC_INIT()            GC_init()
#define GC_MALLOC(sz)        malloc(sz)
#define GC_MALLOC_ATOMIC(sz) malloc(sz)
#define GC_REALLOC(old,sz)   realloc(old,sz)
#define GC_FREE(p)           free(p)

#define GC_malloc(sz)        malloc(sz)
#define GC_realloc(old,sz)   realloc(old,sz)
#define GC_free(p)           free(p)

#define GC_set_warn_proc(p)
#define GC_set_oom_fn(p)

/****************************************************************************
 * Public Types
 ****************************************************************************/

typedef unsigned long GC_word;

/****************************************************************************
 * Public Data
 ****************************************************************************/

extern void *GC_oom_fn;

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

void GC_init(void);

#endif /* __W3MBBUILD_STUBS_GC_H */

