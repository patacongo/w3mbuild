############################################################################
# w3mbuild/Makefile
#
#   Copyright (C) 2018 Gregory Nutt. All rights reserved.
#   Author: Gregory Nutt <gnutt@nuttx.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name NuttX nor the names of its contributors may be
#    used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
# OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
# AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################

TOPDIR =  ${shell pwd}
W3MDIR  = $(TOPDIR)/w3m
IMPORTDIR = $(TOPDIR)/import
INSTALLDIR = $(TOPDIR)/install
BUILDDIR = $(TOPDIR)/build
INCDIR = $(IMPORTDIR)/include
LIBDIR = $(IMPORTDIR)/libs

include $(TOPDIR)/Make.defs

DELIM ?= $(strip /)

# Sub-directories from the NuttX export package

SUBDIRS = arch build include libs startup tmp
FILES = .config System.map User.map

EXPORTZIP = $(wildcard nuttx-export*.zip)

ifeq ($(V),1)
  MKIMPORT = ./mkimport.sh -d -x
else
  MKIMPORT = ./mkimport.sh -x
endif

CFGARG  = --prefix=$(INSTALLDIR) --exec-prefix=$(INSTALLDIR) --oldincludedir=$(INCDIR)
CFGARG += --host=$(HOSTNAME) --enable-image=no --disable-mouse
CFGARG += --disable-m17n --disable-unicode -disable-nls
CFGARG += --disable-sslverify --disable-digest-auth
CFGARG += --without-libiconv-prefix --without-libintl-prefix

ifneq ($(CONFIG_NET_IPv6),y)
CFGARG += --disable-ipv6
endif

all: w3m
.PHONY: cpheaders update clean distclean

$(IMPORTDIR):
	$(Q) mkdir -m 755 $(IMPORTDIR)

$(INCDIR):
	$(Q) mkdir -m 755 $(INCDIR)

$(INSTALLDIR):
	$(Q) mkdir -m 755 $(INSTALLDIR)

import: $(IMPORTDIR) $(EXPORTZIP)
	$(Q) $(MKIMPORT) $(TOPDIR)$(DELIM)$(EXPORTZIP)

$(INCDIR)/gc.h: stubs/gc.h import $(INCDIR)
	$(Q) cp -a stubs/gc.h $(INCDIR)/gc.h

$(INCDIR)/gc_version.h: stubs/gc_version.h import $(INCDIR)
	$(Q) cp -a stubs/gc_version.h $(INCDIR)/gc_version.h

cpheaders: $(INCDIR)/gc.h $(INCDIR)/gc_version.h

$(LIBDIR)/libgc.a: cpheaders stubs/gc.c stubs/gc.h
	$(call COMPILE, stubs/gc.c, stubs/gc$(OBJEXT))
	$(call ARCHIVE, $(LIBDIR)/libgc.a, stubs/gc$(OBJEXT))
	$(call DELFILE, stubs/gc$(OBJEXT))

.initialize:
	$(Q) git submodule init
	$(Q) git submodule update
	$(Q) cd w3m; git checkout nuttx; git pull origin nuttx
	$(Q) touch .initialize

.configure: $(INSTALLDIR)
	$(Q) (export CC='$(CC)'; export CFLAGS='$(CFLAGS)'; \
              export CXX='$(CXX)'; export CXXFLAGS='$(CXXFLAGS)'; \
              export CPPFLAGS='$(CPPFLAGS)'; \
              export LDFLAGS='$(LDFLAGS)'; export LIBS='$(LDLIBS)'; \
              export ac_cv_func_setpgrp_void=yes; \
              cd $(BUILDDIR); $(W3MDIR)/configure $(CFGARG))
	$(Q) touch .configure

w3m: import $(LIBDIR)/libgc.a .initialize .configure
	$(Q) $(MAKE) -C $(BUILDDIR) all

clean:
ifneq (,$(wildcard $(BUILDDIR)/Makefile))
	$(Q) $(MAKE) -C $(BUILDDIR) clean
endif

distclean: clean
	$(foreach SDIR, $(SUBDIRS), $(call DELDIR, $(IMPORTDIR)$(DELIM)$(SDIR)))
	$(foreach FILE, $(FILES), $(call DELFILE, $(IMPORTDIR)$(DELIM)$(FILE)))
	$(call DELDIR, $(IMPORTDIR))
	$(call DELDIR, $(INSTALLDIR))
	$(call DELDIR, $(BUILDDIR)$(DELIM)libwc)
	$(call DELDIR, $(BUILDDIR)$(DELIM)po)
	$(call DELDIR, $(BUILDDIR)$(DELIM)scripts)
	$(call DELDIR, $(BUILDDIR)$(DELIM)w3mimg)
	$(call DELFILE, $(BUILDDIR)$(DELIM)*)
	$(call DELFILE, .initialize)
	$(call DELFILE, .configure)
	$(call DELFILE, config.*)
