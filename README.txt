README
=====

  This is a work in progress.  It is an experiment to determine if I can
  build standard autotool based Linux applications with minimal
  modifications to the system under build.

  This direct provides the configure and build in environment for the
  w3m text-based browser (see https://github.com/tats/w3m).

Configuration
=============

  Required options:

    TCP/IP IPv4 networking (Additional IPv6 is optional)
    CONFIG_LIBC_SETLOCALE=y # setlocale() is required.
    CONFIG_SYSTEM_POPEN=y   # popen() required by w3m
    CONFIG_LIBM=y           # Math library needed
    CONFIG_LIBC_FLOATINGPOINT=y

Build Instructions
==================

  1. Create the NuttX export package:

     $ cd nuttx
     $ make distclean
     $ tools/configure.sh -l stm32f4discovery:netnsh
     $ make export
     $ mv nuttx-export-*.zip ../w3mbuild/.

     NOTE that -l selects Linux.  You may need to use another option.
     Currently the Makefile does not have the proper path conversions
     to support building on Windows using a native Windows toolchain!

  2. Configure and build

     $ cd w3mbuild
     $ make distclean
     $ make

STATUS
======

  2018-08-15:  I make it most of the way through the configuration but fail
    on the first test to see if the compiler can make executables:

      os_bringup.c:(.text+0x1c): undefined reference to `nsh_main'

  2018-08-16:  The problem noted at the end of 08-15 was corrected by
    enclosing the library list with --start-group and --end-group.  The
    configuration script now makes it all the through but fails to create
    a Makefile because it depend on libgc.  GC is the garbage collector and
    is available at https://www.hboehm.inf under an public domain license.

    I added a stub version of gc.h, gc_version.h, and libgc.a.  With some
    additional effort, I was able to successfully complete the configuration
    make and on to the make phase.  The make phase fails due to some
    obvious compatibility issues.  So it looks like the mysterious automake
    stuff is out of the way and I am back to the sane world of normal C
    debug.

  2018-08-17:  There are several compile-related issues that must be resolved
    before a successful build can be obtained:

    1. w3m uses popen() and pclose() extensively.  Implementations of these
       functions were added at apps/system/popen.

       Even though popen/pclose were implemented, in all likelihood, the shell
       commands executed by w3m would be Bash-specific and not usable by the
       NSH shell.

    2. See Remaining Compile-Time Errors Below


Remaining Compile-Time Errors
=============================

  Conflicting Prototypes
  ----------------------

  According to OpenGroup.org, the NuttX version of the prototype is correct:

     void bcopy(const void *, void *, int);
    vs
     static inline void bcopy(FAR const void *b1, FAR void *b2, size_t len)

  According to OpenGroup.org, the NuttX version of the prototype is correct:

     void bzero(void *, int);
    vs
     static inline void bzero(FAR void *s, size_t len)

  This are two unrelated functions that have a simple name collision:

     static int file_read(struct io_file_handle *handle, char *buf, int len);
    vs
     ssize_t file_read(FAR struct file *filep, FAR void *buf, size_t nbytes);

  setjmp.h Issues
  ---------------

  I am not sure why the newlib setjmp.h is not declaring these.  Most likely
  these are not supported by the newlib setjmp.h either but are unique to
  Linux and GLIBC.   These need to be replaced with the more embedded
  friendly jmpbuf, setjmp, and longjmp:

    unknown type name 'sigjmp_buf'
    implicit declaration of function 'sigsetjmp'
    implicit declaration of function 'siglongjmp'

  This is a build-related issues, not a code-level porting issue.

  GC-stub related
  ---------------

  This type needs to be added to the stub implementation of gclib:

    unknown type name 'GC_warn_proc'

  I think it can just be a void *.

  Missing Signal Support
  ----------------------

  The default actions of these signals is not supported by NuttX.  Adding
  arbitrary, unused definitions would eliminate the build errors but would
  not provide the implied underlying functionality:

    'SIGINT' undeclared
    'SIGKILL' undeclared
    'SIGTERM' undeclared
    'SIGILL' undeclared
    'SIGABRT' undeclared
    'SIGFPE' undeclared
    'SIGHUP' undeclared
    'SIGQUIT' undeclared
    'SIGTERM' undeclared

  NOTE: Support for default actions for SIGINT, SIGKILL, SIGTERM, SIGQUIT
  and SIGTERM can be configured into NuttX.  However, their use is
  dangerous in a system based on a CPU with no MMU:  Memory clean-up is
  basically free with processes in the NuttX KERNEL build, but is prone
  to memory leaks in other build configurations.
  
  Enabling cancellation points, however, greatly minimizes the danger.

  Missing struct stat fields
  --------------------------

  These do no apply in NuttX:

    'struct stat' has no member named 'st_ino'
    'struct stat' has no member named 'st_uid'
    'struct stat' has no member named 'st_gid'
    'struct stat' has no member named 'st_dev'

  Missing POSIX support
  ---------------------

  Some of these could be implmented simply in NuttX but some not:

    implicit declaration of function 'utime'
    implicit declaration of function 'getpass'
    implicit declaration of function 'fork'
    implicit declaration of function 'execlp'
    implicit declaration of function 'ctime'
    implicit declaration of function 'setpgid'
    implicit declaration of function 'execl'
    implicit declaration of function 'chmod'
    implicit declaration of function 'geteuid'
    implicit declaration of function 'getegid'
    implicit declaration of function 'ttyname'

  NOTE: Stub versions of geteuid() and getegid() have been added to NuttX
  in recent commits.
  
  newlib Header File Issues
  -------------------------

  NOTE: This error is caused a by including incompatible newlib header
  files.  A dummy sys/param.h and a valid endian.h header files have been
  added to NuttX and should eliminate this error.

    from /usr/include/newlib/sys/param.h ->
      /usr/include/newlib/machine/endian.h ->
        /usr/include/newlib/sys/_types.h:168 ->
          error: unknown type name 'wint_t'

  Miscellaneous
  -------------

    expected identifier or '(' before '{' token
    DEFUN(redoPos, REDO, "Cancel the last undo")
    {

